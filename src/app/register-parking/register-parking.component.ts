import { Component } from '@angular/core';
import { FormBuilder, Validators } from "@angular/forms";
import { Router } from '@angular/router';
import { Parking } from '../model/parking.vehicle';

const PLATE_REGEX = "^[A-Z]{3}(([0-9]{4})|([0-9]{1}[A-Z]{1}[0-9]{2}))"

@Component({
    selector: 'app-register-parking',
    templateUrl: './register-parking.component.html',
    styleUrls: ['./register-parking.component.css']
})
export class RegisterParkingComponent {

    parkingForm = this.formBuilder.group({
        vehicle: ['', [ Validators.required ] ],
        plate: ['', [ Validators.required, Validators.pattern(PLATE_REGEX) ] ],
        driver: ['', [ Validators.required ] ],
        phone: ['', [ Validators.required ] ],
    })

    constructor(private formBuilder: FormBuilder,
                private router: Router) { }

    save() {
        let parking = new Parking(
            this.vehicle.value,
            this.driver.value,
            this.plate.value,
            this.phone.value,
        )

        let list: Parking[] = []
        let storageList = localStorage.getItem('parking-lot')
        if (storageList != null) {
            list = JSON.parse(storageList)   
        }

        list.push(parking)
        localStorage.setItem('parking-lot', JSON.stringify(list))
        
        this.returnToList()
    }

    returnToList() {
        this.router.navigateByUrl('/list')
    }

    get vehicle() { 
        return this.parkingForm.controls.vehicle 
    }

    get plate() { 
        return this.parkingForm.controls.plate 
    }

    get driver() { 
        return this.parkingForm.controls.driver 
    }

    get phone() { 
        return this.parkingForm.controls.phone 
    }

}
