export class Parking {
    vehicle: string
    driver: string
    plate: string
    phone: string
    start: Date
    finish?: Date
    price?: number

    constructor(vehicle: string,
                driver: string,
                plate: string,
                phone: string) {
        this.vehicle = vehicle
        this.driver = driver
        this.plate = plate
        this.phone = phone
        this.start = new Date()
    }
}